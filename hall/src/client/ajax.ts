import { PointLightShadow } from "three";
import { Video, Photo, Utterance } from "interfaces";

export async function getVideoURLData() {
  var urlStr = "../indexes/videoURLData.csv";
  const mediaList: Video[] = [];

  const response = await fetch(urlStr);
  const text = await response.text();

  const allTextLines = text.split(/\r\n|\n/);
  for (var i = 0; i < allTextLines.length; i++) {
    const data = allTextLines[i].split("|");
    const rec: Video = {
      index: parseInt(data[0]),
      youtubeHash: data[1],
      startTimeStr: data[2],
      endTimeStr: data[3],
    };
    mediaList.push(rec);
  }
  return mediaList;
}

export async function getPhotoData() {
  var urlStr = "../indexes/photoData.csv";
  let photoData: Photo[] = [];

  const response = await fetch(urlStr);
  const text = await response.text();

  const allTextLines = text.split(/\r\n|\n/);
  for (var i = 0; i < allTextLines.length; i++) {
    if (allTextLines[i] !== "") {
      var data = allTextLines[i].split("|");
      const rec: Photo = {
        timeStr: data[0],
        filenamePrefix: data[1],
        magNum: data[2],
        credit: data[3],
        description: data[4],
      };
      photoData.push(rec);
    }
  }
  return photoData;
}

export async function getUtteranceData() {
  var urlStr = "../indexes/utteranceData.csv";
  let utteranceData: Utterance[] = [];

  const response = await fetch(urlStr);
  const text = await response.text();

  const allTextLines = text.split(/\r\n|\n/);
  for (var i = 0; i < allTextLines.length; i++) {
    const data = allTextLines[i].split("|");
    if (data[0] !== "") {
      data[1] = data[1].replace(/CDR/g, "Cernan");
      data[1] = data[1].replace(/CMP/g, "Evans");
      data[1] = data[1].replace(/LMP/g, "Schmitt");
      data[1] = data[1].replace(/PAO/g, "Public Affairs");
      // data[1] = data[1].replace(/CC/g, "Mission Control");
      data[2] = data[2].replace(/O2/g, "O<sub>2</sub>");
      data[2] = data[2].replace(/H2/g, "H<sub>2</sub>");
      data[2] = data[2].replace(/Tig /gi, "T<sub>ig</sub> ");
      data[2] = data[2].replace(/Tig\./gi, "T<sub>ig</sub>.");
      data[2] = data[2].replace(/DELTA-VC/gi, "DELTA-V<sub>c</sub>");
      data[2] = data[2].replace(/DELTA-VT/gi, "DELTA-V<sub>t</sub>");
      data[2] = data[2].replace(/VGX /g, "V<sub>gx</sub> ");
      data[2] = data[2].replace(/VGY /g, "V<sub>gy</sub> ");
      data[2] = data[2].replace(/VGZ /g, "V<sub>gz</sub> ");

      const rec: Utterance = {
        timeStr: data[0],
        who: data[1],
        what: data[2],
      };
      utteranceData.push(rec);
    }
  }
  return utteranceData;
}
