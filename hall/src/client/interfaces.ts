export interface Video {
  index: number;
  youtubeHash: string;
  startTimeStr: string;
  endTimeStr: string;
}

export interface Photo {
  timeStr: string;
  filenamePrefix: string;
  magNum: string;
  credit: string;
  description: string;
}

export interface Utterance {
  timeStr: string;
  who: string;
  what: string;
}

export interface MissionData {
  videoURLData: Video[];
  photoData: Photo[];
  utteranceData: Utterance[];
}